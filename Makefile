all: repo com.skype.Client.json
	rm -rf skype
	flatpak-builder --arch=i386 --repo=repo skype com.skype.Client.json

repo:
	ostree init --mode=archive-z2 --repo=repo
